# Bitcoin Kueri

### Sobre o Projeto
Esse projeto foi um desafio técnico da empresa **Hrestar** como a 6ª etapa de um processo seletivo para desenvolvedor web no período de 3 dias e meio. 

O projeto consiste em uma aplicação que lê um range de datas e apresenta um gráfico com as cotações de fechamento do **bitcoin** desse range de datas, escolhido previamente pelo usuário, em reais. Foi utilizado a API **Coinpaprika** para essa consulta.

### Instalação

Para esse projeto você irá precisar, além de um editor de código-fonte ([Visual Studio Code](https://code.visualstudio.com/), por exemplo), do [Node.js](https://nodejs.org/en/) 
 e [Angular](https://angular.io/) instalado e utilizar os seguintes comandos no terminal:

```
npm install

npm install -g @angular/cli
```

### Iniciar aplicação

```
npm start
```

### Contato

 - [GitHub](https://github.com/brunnamattos/)
 - [LinkedIn](https://www.linkedin.com/in/brunna-mattos/)
 - [BitBucket](https://bitbucket.org/brunna-mattos/)
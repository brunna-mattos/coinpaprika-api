import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { Consult } from 'src/app/shared/model/consult.model';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-consult',
  templateUrl: './consult.component.html',
  styleUrls: ['./consult.component.css']
})
export class ConsultComponent implements OnInit {

  form: FormGroup;
  consultHistorical: Consult;

  constructor(
    private fb: FormBuilder,
    public apiService: ApiService,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      dateRange: new FormGroup({
        start: new FormControl(),
        end: new FormControl()
      })
    })
  }

  submit(): void {
    let dateFormatStart: moment.Moment = moment.utc(this.form.value.dateRange.start).local();
    let dateFormatEnd: moment.Moment = moment.utc(this.form.value.dateRange.end).local();

    this.form.value.dateRange.start = dateFormatStart.format("YYYY-MM-DD");
    this.form.value.dateRange.end = dateFormatEnd.format("YYYY-MM-DD");

    this.getHistorical()

  }

  getHistorical() {
    this.apiService.getApiCoinHistorical('historical', this.form.value.dateRange.start, this.form.value.dateRange.end)
      .subscribe(data => {
        this.consultHistorical = data;
      })
  }

}



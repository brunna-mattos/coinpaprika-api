import { Component, OnInit } from '@angular/core';
import { HIGH_CONTRAST_MODE_ACTIVE_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { MatDialog } from '@angular/material/dialog';
import { HomeFormDialogComponent } from './home-form-dialog/home-form-dialog.component';
import { ConsultComponent } from './consult/consult.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    
  }

  openConsult(): void {
    const dialogRef = this.dialog.open(ConsultComponent, {
      minWidth: '300px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  

}
